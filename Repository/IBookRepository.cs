﻿using BookStoreAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStoreAPI.Repository
{
    public interface IBookRepository
    {
        Task<List<Book>> GetAllBooksAsync();
        Task<Book> GetBookByIdAsync(int id);
        Task<List<Book>> GetBookByAuthorAsync(string author);
        Task<int> AddBookAsync(Book books);
        Task<Book> UpdateBookByIdAsync(int id, Book books);
        Task UpdateBookPatchAsync(int id, JsonPatchDocument books);
        Task DeleteBookById(int id);
    }

}
  