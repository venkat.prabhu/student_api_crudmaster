﻿using AutoMapper;
using BookStoreAPI.DataContext;
using BookStoreAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreAPI.Repository
{
    public class BookRepository: IBookRepository
    {
        private readonly BookStoreContext _context;
        private readonly IMapper _mapper;

        public BookRepository(BookStoreContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<Book>> GetAllBooksAsync()
        {
            var records =await _context.Books.Select(x => new Book()
            {
               Id = x.Id,
               Title = x.Title,
               Description = x.Description,
               Price = x.Price,
               NoOfCopies = x.NoOfCopies,
               Author = x.Author
            }).ToListAsync();

            return records;
        }

        public async Task<Book> GetBookByIdAsync(int id) //model changed
        {
            /*var record = await _context.Books.Where(x=>x.Id == id).Select(x => new Book()//methods changed
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                Price = x.Price,
                NoOfCopies = x.NoOfCopies,
                Author = x.Author
            }).FirstOrDefaultAsync();//method changed

            return record;*/

            //using automapper concept
            var book = await _context.Books.FindAsync(id);
            return _mapper.Map<Book>(book);
        }

        public async Task<List<Book>> GetBookByAuthorAsync(string author) //model changed
        {
            var records = await _context.Books.Where(x => x.Author == author).Select(x => new Book()//methods changed
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                Price = x.Price,
                NoOfCopies = x.NoOfCopies,
                Author = x.Author
            }).ToListAsync();//method changed

            return records;
        }

        public async Task<int> AddBookAsync(Book books) //model changed
        {
            var book = new Book()
            {
                Author = books.Author,
                Id = books.Id,
                Description = books.Description,
                NoOfCopies = books.NoOfCopies,
                Price = books.Price,
                Title = books.Title
            };
            _context.Add(book);
            await _context.SaveChangesAsync();

            return book.Id;
        }

        public async Task<Book> UpdateBookByIdAsync(int id,Book books) //model changed
        {
            /* var book = await _context.Books.FindAsync(id);//method changed

             if(book!= null)
             {
                 book.Title = books.Title;
                 book.Description = books.Description;
                 book.NoOfCopies = books.NoOfCopies;
                 book.Author = books.Author;
                 book.Price = books.Price;
                 await _context.SaveChangesAsync();
             }*/
            var book = new Book()
            {
                Id = id,
                Author = books.Author,
                Description = books.Description,
                NoOfCopies = books.NoOfCopies,
                Price = books.Price,
                Title = books.Title
            };
            _context.Update(book);
            await _context.SaveChangesAsync();
            return book;
        }

       public async Task UpdateBookPatchAsync(int id, JsonPatchDocument books)
        { 
            var book = await _context.Books.FindAsync(id);
            if (book != null)
            {
                books.ApplyTo(book);
                await _context.SaveChangesAsync();
            }
        }

        public async Task  DeleteBookById (int id)
        {
            var book = new Book() { Id = id };
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
        }

    }
}
