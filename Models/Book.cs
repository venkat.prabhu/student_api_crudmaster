﻿using System.ComponentModel.DataAnnotations;

namespace BookStoreAPI.Models
{
    public class Book
    {
       public int Id { get; set; }
      
       [Required(ErrorMessage ="title is required")] // model validation
       public string Title { get; set; }
       public string Description { get; set; }

       public string Author { get; set; }

       public int NoOfCopies { get; set; }

       public int Price { get; set; }
    }
}
