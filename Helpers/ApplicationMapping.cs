﻿using AutoMapper;
using BookStoreAPI.Models;

namespace BookStoreAPI.Helpers
{
    public class ApplicationMapping: Profile
    {
        public ApplicationMapping()
        {
            CreateMap<Books, Book>().ReverseMap();
        }
        
    }
}
