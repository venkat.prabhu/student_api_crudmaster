﻿using BookStoreAPI.Models;
using BookStoreAPI.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   
    public class BooksController : ControllerBase
    {
        private readonly IBookRepository _bookRepository;

        public BooksController( IBookRepository BookRepository)
        {
            _bookRepository = BookRepository;
        }

        [HttpGet("GetAll")]
        [Authorize]
        public async Task<IActionResult> GetAllBooks()
        {
            var books = await _bookRepository.GetAllBooksAsync();

            return Ok(books);
        }

        [HttpGet("GetById")]
        [Authorize]
        public async Task<IActionResult> GetBookById(int id)
        {
            var book = await _bookRepository.GetBookByIdAsync(id);
            if(book == null)
            {
                return NotFound();
            }
            return Ok(book);
        }

        [HttpGet("GetByauthor")]
        [Authorize]
        public async Task<IActionResult> GetBooksByAuthor(string author)
        {
            var books = await _bookRepository.GetBookByAuthorAsync(author);

            return Ok(books);
        }

        [HttpPost("AddNewBook")]
        [Authorize]

        public async Task<IActionResult> AddNewBook([FromForm] Book book)
        {
            var id = await _bookRepository.AddBookAsync(book);

            return CreatedAtAction(nameof(GetBookById), new {id = id, Controller = "Books"}, id);
            
        }

        [HttpPut("UpdateBook")]
        [Authorize]
        public async Task<IActionResult> UpdateBook([FromQuery] int id ,[FromForm] Book book)
        {
            var books = await _bookRepository.UpdateBookByIdAsync(id,book);
            return Ok(books);
        }

        [HttpPatch("UpdateBookPatch")]
        [Authorize]
        public async Task<IActionResult> UpdateBookPatch([FromQuery] int id, [FromBody] JsonPatchDocument book)
        {
            await _bookRepository.UpdateBookPatchAsync(id, book);
            return Ok();
        }
        [HttpDelete("DeleteBook")]
        [Authorize]
        public async Task<IActionResult> DeleteBook([FromQuery] int id)
        {
            await _bookRepository.DeleteBookById(id);
            return Ok();
        }


    }
}
