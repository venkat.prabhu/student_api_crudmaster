﻿using BookStoreAPI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookStoreAPI.DataContext
{
    public class BookStoreContext:IdentityDbContext<UserIdentity>

    {
        public BookStoreContext(DbContextOptions<BookStoreContext> options):base(options)
        {

        }
        public DbSet<Book> Books { get; set; }


        //one method to declare connection string
        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=BookStoreAPI;Integrated Security=True");
            base.OnConfiguring(optionsBuilder);
        }*/
    }
}
